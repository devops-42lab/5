TAG="deploq"

while getopts t: flag
do
    case "${flag}" in
        t) TAG=${OPTARG};;
    esac
done

export TAG=$TAG
docker-compose up -d --build
