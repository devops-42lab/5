import logging
import socket
import json
import threading


class SocketHandlerWithSocket(logging.Handler):
    def __init__(self, host, port, datefmt=None):
        super().__init__()
        self.host = host
        self.port = port
        self.socket = None
        self.lock = threading.Lock()
        self.datefmt = datefmt

    def makeSocket(self):
        if self.socket is None:
            self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.socket.connect((self.host, self.port))
        return self.socket

    def send(self, data):
        try:
            with self.lock:
                sock = self.makeSocket()
                sock.sendall(data.encode())
        except Exception as e:
            print(f"Failed to send log data: {e}")

    def format(self, record):
        formatter = logging.Formatter(datefmt=self.datefmt)
        formatted_record = formatter.formatTime(record, self.datefmt)
        log_record = {
            'timestamp': formatted_record,
            'level': record.levelname,
            'message': record.getMessage(),
            'logger': record.name,
            'pathname': record.pathname,
            'lineno': record.lineno,
            'funcName': record.funcName,
            'process': record.process,
            'thread': record.thread
        }
        return json.dumps(log_record)

    def emit(self, record):
        try:
            log_entry = self.format(record)
            self.send(log_entry)
        except Exception as e:
            self.handleError(record)

    def close(self):
        with self.lock:
            if self.socket is not None:
                self.socket.close()
                self.socket = None
        super().close()
