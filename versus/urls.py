from django.urls import path
from .views import split_sentences_view, pos_coloring_view, syntax_comparison, extract_entities_view

urlpatterns = [
    path('', split_sentences_view, name='split_sentences'),
    path('pos/', pos_coloring_view, name='pos_coloring'),
    path('syntax/', syntax_comparison, name='syntax_comparison'),
    path('ner/', extract_entities_view, name='extract_entities'),
]
