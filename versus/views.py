import spacy
import pymorphy2
import time
# import logging
from django.shortcuts import render
from .forms import TextInputForm
from natasha import Segmenter, MorphVocab, NewsEmbedding, NewsNERTagger, NewsSyntaxParser, NewsMorphTagger, Doc


# logger = logging.getLogger(__name__)


def extract_entities(text):
    # spaCy
    nlp_spacy = spacy.load('ru_core_news_sm')
    # logger.debug("SALAM. Начинаем работу. Вытаскиваем данные")
    doc_spacy = nlp_spacy(text)
    spacy_entities = [(ent.text, ent.label_) for ent in doc_spacy.ents]
#     logger.debug("Вытащили предложения для SpaCy. Количество предложений: %s", spacy_entities)

    # Natasha
    segmenter = Segmenter()
    morph_vocab = MorphVocab()
    emb = NewsEmbedding()
    ner_tagger = NewsNERTagger(emb)
    doc_natasha = Doc(text)
    doc_natasha.segment(segmenter)
    doc_natasha.tag_ner(ner_tagger)
    natasha_entities = [(ent.text, ent.type) for ent in doc_natasha.spans]
#     logger.debug("Вытащили предложения для Natasha. Количество предложений: %s", natasha_entities)

    return {
        'spacy': spacy_entities,
        'natasha': natasha_entities
    }


def extract_entities_view(request):
#     logger.debug("Жестко принимаем данные, отправляем их на обработку и потом выводим на странице вместе с рендерингом")
    if request.method == 'POST':
        form = TextInputForm(request.POST)
        if form.is_valid():
            text = form.cleaned_data['text']
            results = extract_entities(text)
            return render(request, 'entities.html', {'form': form, 'results': results})
    else:
        form = TextInputForm()
    return render(request, 'index.html', {'form': form})


def split_sentences(text):
#     logger.debug("Начинаем разбиение предложений")
    nlp_spacy = spacy.load('ru_core_news_sm')
    segmenter = Segmenter()
    start_time = time.time()
    doc_spacy = nlp_spacy(text)
    spacy_sentences = list(doc_spacy.sents)
    spacy_time = time.time() - start_time

    start_time = time.time()
    doc_natasha = Doc(text)
    doc_natasha.segment(segmenter)
    natasha_sentences = [_.text for _ in doc_natasha.sents]
    natasha_time = time.time() - start_time

#     logger.debug("Завершили разбиение предложений. Результаты: SpaCy - %s, Natasha - %s", spacy_sentences,
#                  natasha_sentences)

    return {
        'spacy': {
            'sentences': spacy_sentences,
            'time': spacy_time
        },
        'natasha': {
            'sentences': natasha_sentences,
            'time': natasha_time
        }
    }


def pos_coloring(text):
#     logger.debug("Начинаем разметку частей речи")
    nlp_spacy = spacy.load('ru_core_news_sm')
    morph_vocab = MorphVocab()
    emb = NewsEmbedding()
    morph_tagger = NewsMorphTagger(emb)
    syntax_parser = NewsSyntaxParser(emb)
    doc_spacy = nlp_spacy(text)
    spacy_pos_colored = ''.join([f'<span class="{token.pos_}">{token.text}</span> ' for token in doc_spacy])
    doc_natasha = Doc(text)
    doc_natasha.segment(Segmenter())
    doc_natasha.tag_morph(morph_tagger)
    natasha_pos_colored = ''
    for token in doc_natasha.tokens:
        natasha_pos_colored += f'<span class="{token.pos}">{token.text}</span> '

#     logger.debug("Завершили разметку частей речи. Результаты: SpaCy - %s, Natasha - %s", spacy_pos_colored,
#                  natasha_pos_colored)

    return {
        'spacy': spacy_pos_colored,
        'natasha': natasha_pos_colored
    }


def split_sentences_view(request):
    if request.method == 'POST':
        form = TextInputForm(request.POST)
        if form.is_valid():
            text = form.cleaned_data['text']
            results = split_sentences(text)
            return render(request, 'sentences.html', {'form': form, 'results': results})
    else:
        form = TextInputForm()

    return render(request, 'index.html', {'form': form})


def pos_coloring_view(request):
    if request.method == 'POST':
        form = TextInputForm(request.POST)
        if form.is_valid():
            text = form.cleaned_data['text']
            results = pos_coloring(text)
            return render(request, 'pos.html',
                          {'form': form, 'results': results, 'errors': {'spacy': 0, 'natasha': 0}})
    else:
        form = TextInputForm()

    return render(request, 'index.html', {'form': form})


def dependency_parsing(text):
#     logger.debug("Начинаем разбор зависимостей")
    nlp_spacy = spacy.load('ru_core_news_sm')
    doc_spacy = nlp_spacy(text)
    spacy_dependencies = []
    for token in doc_spacy:
        spacy_dependencies.append({
            'text': token.text,
            'dep': translate_dependency(token.dep_),
            'head': token.head.text
        })

#     logger.debug("Завершили разбор зависимостей. Результаты: SpaCy - %s", spacy_dependencies)

    return {
        'spacy': spacy_dependencies
    }


def translate_dependency(dep):
    dependency_dict = {
        'ROOT': 'Корень',
        'acl': 'Зависимое слово',
        'acomp': 'Определение',
        'advcl': 'Обстоятельство',
        'advmod': 'Модификатор',
        'agent': 'Агент',
        'amod': 'Определение',
        'appos': 'Приложение',
        'attr': 'Атрибут',
        'aux': 'Вспомогательный глагол',
        'auxpass': 'Вспомогательный глагол (страдательный залог)',
        'case': 'Падеж',
        'cc': 'Союз',
        'ccomp': 'Комплемент',
        'compound': 'Сложный компонент',
        'conj': 'Соединение',
        'csubj': 'Клауза-субъект',
        'csubjpass': 'Клауза-субъект (страдательный залог)',
        'dative': 'Дательный падеж',
        'dep': 'Зависимость',
        'det': 'Детерминатив',
        'dobj': 'Прямое дополнение',
        'expl': 'Эксплетив',
        'intj': 'Междометие',
        'mark': 'Маркер',
        'meta': 'Мета',
        'neg': 'Отрицание',
        'nmod': 'Модификатор имени',
        'nounmod': 'Модификатор существительного',
        'npmod': 'Модификатор именной группы',
        'nummod': 'Числовое определение',
        'nsubj': 'Субъект',
        'oprd': 'Объект после глагола-связки',
        'obj': 'Объект',
        'obl': 'Объектный модификатор',
        'parataxis': 'Паратаксис',
        'punct': 'Пунктуация',
        'relcl': 'Относительная клауза',
        'reparandum': 'Восстанавливаемое слово',
        'root': 'Корень',
        'vocative': 'Обращение',
        'xcomp': 'Неполный глагольный комплемент'
    }
    return dependency_dict.get(dep, dep)


def dependency_parsing_view(request):
    if request.method == 'POST':
        form = TextInputForm(request.POST)
        if form.is_valid():
            text = form.cleaned_data['text']
            results = dependency_parsing(text)
            return render(request, 'dependencies.html', {'form': form, 'results': results})
    else:
        form = TextInputForm()

    return render(request, 'index.html', {'form': form})


def syntax_comparison(request):
    if request.method == 'POST':
        form = TextInputForm(request.POST)
        if form.is_valid():
            text = form.cleaned_data['text']

            # spaCy
            nlp_spacy = spacy.load("ru_core_news_sm")
            doc_spacy = nlp_spacy(text)
            spacy_results = []
            for token in doc_spacy:
                spacy_results.append({
                    "text": token.text,
                    "lemma": token.lemma_,
                    "pos": token.pos_,
                    "dep": token.dep_,
                })

            # Natasha
            segmenter = Segmenter()
            emb = NewsEmbedding()
            morph_tagger = NewsMorphTagger(emb)
            syntax_parser = NewsSyntaxParser(emb)
            doc_natasha = Doc(text)
            doc_natasha.segment(segmenter)
            doc_natasha.tag_morph(morph_tagger)
            doc_natasha.parse_syntax(syntax_parser)
            natasha_results = []
            for token in doc_natasha.tokens:
                natasha_results.append({
                    "text": token.text,
                    "lemma": token.lemma,
                    "pos": token.pos,
                    "dep": token.rel,
                })

            return render(request, 'syntax_comparison.html',
                          {'form': form, 'spacy_results': spacy_results, 'natasha_results': natasha_results})
    else:
        form = TextInputForm()

    return render(request, 'index.html', {'form': form})
