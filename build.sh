TAG="build"

while getopts t: flag
do
    case "${flag}" in
        t) TAG=${OPTARG};;
    esac
done

docker build -t my_django_app:$TAG .
